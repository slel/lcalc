#!/bin/sh
#
# Compare the first nine (complex parts of the) zeros of the Riemann
# zeta function to a precomputed list. This is one of the lcalc
# examples from our documentation.

# http://www.dtc.umn.edu/~odlyzko/zeta_tables/zeros2
EXPECTED="14.134725141
21.022039638
25.010857580
30.424876125
32.935061587
37.586178158
40.918719012
43.327073280
48.005150881"

ACTUAL=$("${lcalc}" -z 9)

printf "Testing the output of lcalc -z 9... "

. "${testlib}/compare_fp_lists.sh"
compare_fp_lists "${EXPECTED}" "${ACTUAL}"
